k = 2:
----------
cluster 0:
	Points:
		[68.0, 99.0]
		[33.0, 85.0]
		[28.000000000000004, 71.0]
		[44.0, 23.0]
		[3.0, 97.0]
		[43.0, 68.0]
		[51.0, 83.0]
		[7.000000000000001, 61.0]
		[42.0, 35.0]
		[80.0, 46.0]
		[48.0, 14.000000000000002]
		[63.0, 91.0]
		[25.0, 78.0]
		[69.0, 24.0]
		[48.0, 90.0]
		[43.0, 6.0]
		[24.0, 35.0]
		[78.0, 5.0]
	Max:
		 Cluster pair = (cluster 0, cluster 1)
		 Cluster pair distance = 106.89246933250256
	Min:
		 Cluster pair = (cluster 0, cluster 1)
		 Cluster pair distance = 5.0990195135927845
	Intracluster distance:
		869.8916794170742

cluster 1:
	Points:
		[19.0, 4.0]
		[64.0, 23.0]
	Max:
		 Cluster pair = (cluster 1, cluster 0)
		 Cluster pair distance = 106.89246933250256
	Min:
		 Cluster pair = (cluster 1, cluster 0)
		 Cluster pair distance = 5.0990195135927845
	Intracluster distance:
		48.84669896727925

Total Distance between points = 918.7383783843535

k = 4:
----------
cluster 0:
	Points:
		[68.0, 99.0]
		[33.0, 85.0]
		[28.000000000000004, 71.0]
		[44.0, 23.0]
		[3.0, 97.0]
		[43.0, 68.0]
		[51.0, 83.0]
		[7.000000000000001, 61.0]
		[42.0, 35.0]
		[80.0, 46.0]
		[48.0, 14.000000000000002]
		[63.0, 91.0]
	Max:
		 Cluster pair = (cluster 0, cluster 1)
		 Cluster pair distance = 118.69709347747316
	Min:
		 Cluster pair = (cluster 0, cluster 1)
		 Cluster pair distance = 7.615773105863909
	Intracluster distance:
		510.2205003699775

cluster 1:
	Points:
		[25.0, 78.0]
		[69.0, 24.0]
		[48.0, 90.0]
		[43.0, 6.0]
		[24.0, 35.0]
		[78.0, 5.0]
	Max:
		 Cluster pair = (cluster 1, cluster 0)
		 Cluster pair distance = 118.69709347747316
	Min:
		 Cluster pair = (cluster 1, cluster 3)
		 Cluster pair distance = 5.0990195135927845
	Intracluster distance:
		319.5090077910503

cluster 2:
	Points:
		[19.0, 4.0]
	Max:
		 Cluster pair = (cluster 2, cluster 0)
		 Cluster pair distance = 106.89246933250256
	Min:
		 Cluster pair = (cluster 2, cluster 1)
		 Cluster pair distance = 24.08318915758459
	Intracluster distance:
		0

cluster 3:
	Points:
		[64.0, 23.0]
	Max:
		 Cluster pair = (cluster 3, cluster 0)
		 Cluster pair distance = 95.90099061010788
	Min:
		 Cluster pair = (cluster 3, cluster 1)
		 Cluster pair distance = 5.0990195135927845
	Intracluster distance:
		0

Total Distance between points = 829.7295081610278

